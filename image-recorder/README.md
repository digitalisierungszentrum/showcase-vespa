# Vespa Image Recorder 

Die Hauptanwendung für den Vespa-Showcase. Erlaubt sowohl die Generierung von Bildern für das Trainieren der Modelle für die Erkennung von Defekten als auch für die Live-Qualitäts-Kontrolle. Fürs Trainieren ist die idee, dass das Vespa-modell in einen definierten Zustand (Korrekt oder bestimmter Defekt) gebracht wird und dann eine Anzahl an Bildern basierend auf den Beleuchtungseinstellung und bestimmten Drehositionen erzeugt werden. Diese werden dann zum Trainieren des KI-Modells verwendet.

# Konfiguration der Aufnahmen

Beleuchtung

Dreh-Positionen

# Starten der Anwendung für die verschiedenen Modi

## Aufnahme für KI Training

## Aufnahme für Live-Bestimmung 

# Python Libraries

Wird die Anwendung auf einem Jetson Nano ausgeführt, so sind die benötigten Libraries schon vorinstalliert. Ansosnten ist vor allem ```opencv``` zu installieren.