import sys
import json
import os
import shutil
import datetime
import time 

from camera import CameraCapture
from esp32_client import SerialClient

def read_json(file_path):
    try:
        with open(file_path, 'r') as json_file:
            data = json.load(json_file)
            return data
    except:
        print(f"An error occurred while reading the file {file_path}")

def write_json(file_path, content):
    try:
        with open(file_path, 'w') as json_file:
            json.dump(content, json_file, indent=2)
    except:
        print(f"An error occurred while writing the file {file_path}")


def main():
    if len(sys.argv) < 2:
        print("Please provide a name as an argument.")
        return

    name = sys.argv[1]

    print(f"Starting capture of case {name}...")

    config = read_json("./config.json")

    print(config)

    directory = f"./output/{name}"
    if os.path.exists(directory):
        print(f"delete directory {directory}")
        shutil.rmtree(directory)

    # create directory
    os.makedirs(directory)

    # loop over colors
    current_time_str = datetime.datetime.today().strftime("%Y-%m-%d")
    file_suffix = f"{directory}/{current_time_str}_vespa_"
    current_image_number = 0

    serial_client = SerialClient("/dev/ttyUSB0")
    serial_client.open()
    camera = CameraCapture(directory)
    camera.start_camera()
    try:
        numberOfImages = config["numberOfImages"]
        degreesPerImage = 360 / numberOfImages
        print(f"take {numberOfImages} ({degreesPerImage} per image)")

        # loop over rotation (number of images per 360degree)
        for index in range(numberOfImages):
            serial_client.send_rotation(int(index * degreesPerImage))
            time.sleep(5)

            for color in config["colors"]:
                print(color)
                # set light parameters
                serial_client.send_color(color)
                
                time.sleep(1)

                print(f"image {color}_{index} (degrees: {index * degreesPerImage})")
                file_name = f"{file_suffix}{current_image_number:03d}"
                # take and save image
                
                camera.capture_frame(f"{file_name}.jpg")
               
                # generate json with parameters + timestamp
                write_json(f"{file_name}-meta.json", {
                    "color": color,
                    "rotation": index * degreesPerImage
                })

                current_image_number += 1
        
        serial_client.send_color("#000000")
        print("DONE")
    finally:
        camera.stop_camera()
        serial_client.close()



if __name__ == "__main__":
    main()
