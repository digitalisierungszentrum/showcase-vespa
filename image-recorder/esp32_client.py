import serial
import time

class SerialClient:
    def __init__(self, port):
        self.port = port
        self.client : serial.Serial = None

    def open(self):
        self.client = serial.Serial(self.port, baudrate=115200)

    def close(self):
        self.client.close()

    def send_color(self, color):
        if self.client and self.client.is_open:
            print(f"send new color {color}")

            self.client.read_all()

            self.client.write(f"C{color}\r\n".encode(encoding = 'ascii', errors = 'strict'))
        else:
            print("serial client ios closed")

    def send_rotation(self, angle):
        if self.client and self.client.is_open:
            print(f"send new angle {angle}")

            self.client.read_all()

            self.client.write(f"A{angle}\r\n".encode(encoding = 'ascii', errors = 'strict'))
        else:
            print("serial client ios closed")
