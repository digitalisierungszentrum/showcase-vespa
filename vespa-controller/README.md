# Vespa Controller - Aufgabe und Übersicht

Mit dem Vespa Controller sollen wvordefinierte Bilder der Vespa auf einem Drehteller erstellt werden. Der Controller übernimmt hierbei die Aufgabe, den Drehteller auf definierte Positionen zu drehen und die gewünschte Beleuchtung einzustellen.

Die Erstellung der Bilder erfolgt hierbei nicht über den Controller sondern über die parallel entwickelte Python Anwendung. Ebenso erfogt die Ansteuerung des Controllers ausgehend von der Python Anwendung über die serielle Schnittstelle. Die Kommunikation ist unter [Kommunikation](#kommunikation) beschrieben.

# Vorbereitung und Installation

Für die Erstellung wird Visual Studio mit Platform IO verwendet. Somit muss dieses installiert sein. Der Controller läuft auf dienem Esp32, welcher vorhanden und in Platform IO installiert sein muss.

Danach muss nur noch das Projekt in Visual Studio Code geöffnet werden und kann dann gebaut und per USB Kabel auf den ESP32 gespielt werden.

# Kommunikation

Derzeit stehen zwei Kommandos zu Verfügung. Diese werden per serieller Schnittstelle über das USB Kabel empfangen Hierbei werden die Kommandos über ein Startzeichen gekennzeichnet. Die Kommandos sind:

1. Ändern der Beleuchtung über das Präfix ```C```.
2. Drehen des Tellers auf eine gewünschte Position über das Präfix ```A```.

Um eine Kommando abzuschließen muss neben der reinen Zeichenfolge immer noch ein Zeilenumbruch ```\r\n``` geschickt werden. 

Die Kommandos und deren Parameter werden im fogenden kurz erklärt.

## Beleuchtung ändern

Die gewünschte Beleuchtung wird über das Präfix ```C``` gestartet. Danach kommen 6 Zeichen für die RGB Werte im Hex Format.

```
Crrggbb
```

Somit ist volle Helligkeit in weiß über ```CFFFFFF``` zu setzen. Aber auch beliebige Farben können gesetzt werden, z.B. rot über ```CFF0000```.

Um die Beleuchtung wieder auszuschalten kann über ```C000000``` die Beleuchtung auf Schwarz gesetzt werden.

## Drehteller auf Position bewegen

Wird der Controller initialisiert, wird der Drehteller ebenfalls initialisiert und auf 0° gedreht. Danach kann über ```Axxx``` der Teller auf eine bestimmte Gradzahl gedreht werden. Für 90° muss also ```A90``` über die serielle Schnittstelle geschickt werden.

# Bauteile ud Verkabelung

- ESP32
- Neopixel-Ring von beliebiger Größe (Anzahl der Pixel gegebenenfalls anpassen)
- Stepper-Treiber (z.B. A4988 oder DRV8825) mit passender Stromversorgung