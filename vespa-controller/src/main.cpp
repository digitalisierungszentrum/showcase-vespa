#include <Arduino.h>

#include <Adafruit_NeoPixel.h>

#define PIN_LED 2
#define PIN_STEP 4
#define PIN_DIR 0
#define PIN_LIMIT 16

#include <ESP_FlexyStepper.h>

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 12

const int DISTANCE_TO_TRAVEL_IN_STEPS = 2000;
const int SPEED_IN_STEPS_PER_SECOND = 300;
const int ACCELERATION_IN_STEPS_PER_SECOND = 200;
const int DECELERATION_IN_STEPS_PER_SECOND = 200;

const float STEPS_PER_DEGREE = 5.56f;

ESP_FlexyStepper stepper;

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_LED, NEO_GRB + NEO_KHZ800);

int delayval = 250; // delay for half a second

const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data
char command;

long currentPosition = 0;

boolean newData = false;

void recvWithEndMarker()
{
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;

  // if (Serial.available() > 0) {
  while (Serial.available() > 0 && newData == false)
  {
    rc = Serial.read();

    if (rc != endMarker)
    {
      receivedChars[ndx] = rc;
      ndx++;
      if (ndx >= numChars)
      {
        ndx = numChars - 1;
      }
    }
    else
    {
      receivedChars[ndx] = '\0'; // terminate the string
      ndx = 0;
      newData = true;

      command = receivedChars[0];

      Serial.print("recevied command ");
      Serial.print(command);
      Serial.print(": ");
      Serial.println(receivedChars);
    }
  }
}

void updateColors()
{
  if (newData == true)
  {
    long int rgb = strtol(receivedChars + 1, 0, 16); //=>rgb=0x001234FE;
    byte r = (byte)(rgb >> 16);
    byte g = (byte)(rgb >> 8);
    byte b = (byte)(rgb);
    Serial.print("R: ");
    Serial.println(r);
    Serial.print("G: ");
    Serial.println(g);
    Serial.print("B: ");
    Serial.println(b);

    Serial.print("Set new color ... ");
    Serial.println(receivedChars);
    for (int i = 0; i < NUMPIXELS; i++)
    {
      pixels.setPixelColor(i, pixels.Color(r, g, b));
      pixels.show();
    }
    newData = false;
  }
}

void setup()
{
  Serial.begin(115200);

  pinMode(PIN_LIMIT, INPUT_PULLUP);

  Serial.println("Init stepper...");
  // connect and configure the stepper motor to its IO pins
  stepper.connectToPins(PIN_STEP, PIN_DIR);
  // set the speed and acceleration rates for the stepper motor
  stepper.setSpeedInStepsPerSecond(SPEED_IN_STEPS_PER_SECOND);
  stepper.setAccelerationInStepsPerSecondPerSecond(ACCELERATION_IN_STEPS_PER_SECOND);
  stepper.setDecelerationInStepsPerSecondPerSecond(DECELERATION_IN_STEPS_PER_SECOND);


  // Not start the stepper instance as a service in the "background" as a separate task
  // and the OS of the ESP will take care of invoking the processMovement() task regularily so you can do whatever you want in the loop function
  stepper.startAsService(1);

  delay(500);
  stepper.moveToHomeInSteps(1, 200, 2000, PIN_LIMIT);

  Serial.println("Init LEDs...");

  pixels.begin(); // This initializes the NeoPixel library.

  delay(500);

  Serial.println("Started, wait for commands...");
}

void moveToPosition()
{
  long int angle = strtol(receivedChars + 1, 0, 10);
  Serial.print("Move to position ");
  Serial.println(angle);

  long int diff = angle - currentPosition;
  while (diff < 0) {
    diff += 360;
  }

  long int steps_to_move = (int)roundf(diff * STEPS_PER_DEGREE);
  Serial.print("Move steps: ");
  Serial.println(steps_to_move);

  stepper.setTargetPositionRelativeInSteps(steps_to_move);

  currentPosition = angle;

  // stepper.goToLimitAndSetAsHome()

  // if (stepper.getDistanceToTargetSigned() == 0)
  // {
  //   delay(1000);
  //   long relativeTargetPosition = DISTANCE_TO_TRAVEL_IN_STEPS;
  //   Serial.printf("Moving stepper by %ld steps\n", relativeTargetPosition);
  //   stepper.setTargetPositionRelativeInSteps(relativeTargetPosition);
  // }
}

void loop()
{
  recvWithEndMarker();
  if (command == 'C')
  {
    updateColors();
  }
  else if (command == 'A')
  {
    moveToPosition();
  }

  newData = false;
  command = '\0';

  Serial.println(digitalRead(PIN_LIMIT));
}